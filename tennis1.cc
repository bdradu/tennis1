#include "tennis.h"

static const int minimumPointsToWin = 4;

std::string getDisplayableScoreWhenTie(const int score)
{

	switch (score)
	{
	case 0:
		return "Love-All";
	case 1:
		return "Fifteen-All";
	case 2:
		return "Thirty-All";
	default:
		return "Deuce";
	}
}

bool isAdvantageOrWin(int firstPlayerScore, int secondPlayerScore)
{
	return (firstPlayerScore >= 4 || secondPlayerScore >= 4);
}

std::string getDisplayableScoreWhenAdvantageOrWin(int firstPlayerScore, int secondPlayerScore)
{

	int minusResult = firstPlayerScore - secondPlayerScore;
	if (minusResult == 1)
		return  "Advantage player1";
	else if (minusResult == -1)
		return "Advantage player2";
	else if (minusResult >= 2)
		return "Win for player1";
	else
		return "Win for player2";
}

std::string scoreToString(const int score)
{
	switch (score)
	{
	case 0:
		return "Love";
	case 1:
		return "Fifteen";
	case 2:
		return "Thirty";
	case 3:
		return "Forty";
	default:
		return "";
	}
}

std::string getDisplayableScore(int firstPlayerScore, int secondPlayerScore)
{
	return scoreToString(firstPlayerScore) + "-" + scoreToString(secondPlayerScore);
}


bool isMinimumPointsToWinReached(int firstPlayerScore, int secondPlayerScore)
{
	return firstPlayerScore >= 4 || secondPlayerScore >= 4;
}

bool isWin(int firstPlayerScore, int secondPlayerScore)
{
	bool differenceBetweenPlayerScoreIsTwo = abs(firstPlayerScore - secondPlayerScore) > 1;
	return isMinimumPointsToWinReached(firstPlayerScore, secondPlayerScore) && differenceBetweenPlayerScoreIsTwo;
}
bool isAdvantage(int firstPlayerScore, int secondPlayerScore)
{
	bool differenceBetweenPlayerScoreIsTwo = abs(firstPlayerScore - secondPlayerScore) == 1;
	return isMinimumPointsToWinReached(firstPlayerScore, secondPlayerScore) && differenceBetweenPlayerScoreIsTwo;
}

bool isTie(int firstPlayerScore, int secondPlayerScore)
{
	return firstPlayerScore == secondPlayerScore;
}

std::string getDisplayableScoreWhenAdvantage(int firstPlayerScore, int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Advantage player1";
	return "Advantage player2";
}
std::string getDisplayableScoreWhenWin(int firstPlayerScore, int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Win for player1";
	return "Win for player2";
}

const std::string tennis_score(int firstPlayerScore, int secondPlayerScore)
{
	if (isTie(firstPlayerScore, secondPlayerScore))
		return getDisplayableScoreWhenTie(firstPlayerScore);

	if (isAdvantage(firstPlayerScore, secondPlayerScore))
		return getDisplayableScoreWhenAdvantage(firstPlayerScore, secondPlayerScore);
	if (isWin(firstPlayerScore, secondPlayerScore))
		return getDisplayableScoreWhenWin(firstPlayerScore, secondPlayerScore);
	//else if (isAdvantageOrWin(firstPlayerScore, secondPlayerScore))
	//	return getDisplayableScoreWhenAdvantageOrWin(firstPlayerScore, secondPlayerScore);

	return getDisplayableScore(firstPlayerScore, secondPlayerScore);
}